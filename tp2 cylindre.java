package tp2;

public class Cylindre extends Cercle {
	private double hauteur;
	
	Cylindre(){
		hauteur = 1.0;
		
	}
	
	double getHauteur() {
		return hauteur;
		
	}
	
	void setHauteur(double h) {
		hauteur = h;
	}
	
	String seDecrireCylindre() {
		return "une hauteur de " + hauteur + super.seDecrire();
		
	
		
	}
	
	Cylindre(String couleur, boolean coloriage , double rayon, double h){
		super(rayon,couleur,coloriage);
		hauteur = h;
		
	}
	
	double calculerVolume() {
		return super.calculerAire() * hauteur;
	}

}
