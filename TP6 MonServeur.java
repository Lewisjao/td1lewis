package tp6;

	import java.io.BufferedReader; // importation de la libariaire

	import java.io.InputStreamReader;
	import java.net.*;

	public class MonServeur {

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			/*Exercice3.1*/
			try{//Try permet de tester
				ServerSocket socketEcoute = new ServerSocket(8888); 
				System.out.println("ServerSocket: " + socketEcoute);
				/*Exercice3.2*/
				Socket service = socketEcoute.accept();
				System.out.println("Le client s'est connecté");
				//Lecture du flux
				/*Exercice3.3*/
				BufferedReader bf = new BufferedReader( new InputStreamReader(service.getInputStream()));
				String message = bf.readLine();
				System.out.println("Message : " + message);
				socketEcoute.close();/*Ce qui permet de fermer le serveur*/
			}
			catch (Exception e){//catch permet d'emprisonner les erreures. 
				System.out.println("Exception : " + e.getMessage());// Ce qui permet d'afficher le message d'erreur.
			}

		}
	}
