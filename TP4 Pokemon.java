package tp4;

public class Pokemon {private String nom;
private int energie;
private int maxEnergie;
private double cycle;
private int puissance;

Pokemon(String n){
	maxEnergie = 50 + (int)(Math.random() * ((90 - 50) + 1));
	energie = 30 + (int)(Math.random() * ((maxEnergie - 30) + 1));
	puissance = 3 + (int)(Math.random() * ((10 - 3) + 1));
	nom = n;
}
String getNom() {
	return nom;
}

void setNom(String n) {
	nom = n;
}

int getEnergie() {
	if(energie < 0)
		energie = 0;
	return energie;
}

int getPuissance() {
	return puissance;
}

void sePresenter() {
	System.out.println("Je suis " + nom + ", j'ai " + energie + " points d'energie (" + maxEnergie + "max) et une puissance de " + puissance);
}

void manger() {
	int potion = 10 + (int)(Math.random() * ((30 - 10) + 1));
	 if(energie > 0 && energie+potion <= maxEnergie) {
		 energie += potion;
	 }
	 cycle += 0.5;
}

void vivre() {
	int vie;
	cycle += 0.5;
	do{
		vie = 20 + (int)(Math.random() * ((40 - 20) + 1));
		energie -= vie;
	}while((energie -= vie) < 0);
}

	void perdreEnergie(int perte) {
		if(energie < maxEnergie)
			perte *= 1.5;
		energie -= perte;
	}
	
	void attaquer(Pokemon adversaire) {
		int fatigue;
			do {
			fatigue = (int)(Math.random() * ((1 - 0) + 1 ));
				}while(puissance - fatigue <= 1);
		puissance -= fatigue;
			if(energie < 0.25*maxEnergie) {
				puissance *= 2;
				System.out.print("(Furie !) ");
				}
		adversaire.perdreEnergie(puissance);
		}
	
void cycleDeVie(Pokemon n) {
	System.out.println(n.nom + " a deja vécu " + (int)cycle + " cycle(s) !");
}

boolean isAlive() {
	if(energie>0)
		return true;
	else {
		energie = 0;
		return false;
	}

}}
