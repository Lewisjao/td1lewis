package tp6;
	import java.io.PrintWriter;
	import java.net.Socket;

	public class MonClient {

		public static void main(String[] args) {
			// TODO Auto-generated method stub

			try{
				Socket client = new Socket("localhost", 8888);/*Ont déclare un objet client*/
				System.out.println("Client: " + client);// Ce qui permet d'afficher l'adresse du client.

				System.out.println("Le client s'est connecté");
				PrintWriter writer = new PrintWriter(client.getOutputStream());/*Ce qui permet au client d'envoyerdes écritures sous forme de texte.j'ai */
				System.out.println("Envoi du message : Hello World");
				writer.println("Hello World");// Ce qui permet au client d'envoyer le message "hello world". 
				writer.flush(); // Cela permet de vider le buffer dans mon serveur.
				client.close(); // Cela permet de fermer la connexion entre le client et le serveur.
			}
			catch(Exception e){
				System.out.println("Exception : " + e.getMessage());// Ce qui capture le message d'erreur et l'affiche dans la console 
			}

		}

	}
