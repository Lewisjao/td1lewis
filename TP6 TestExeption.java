package tp6;

public class TestExceptions {

		public static void main(String[] args) {
			// TODO Auto-generated method stub
			/*Exercice 1.1*/

			/*int x = 2, y = 0;
			System.out.println(x/y);
			System.out.println("Fin du programme");*/
			/* Ce programme ne fonctionne pas car on ne peut pas diviser par 0.*/

			/*Exercice 1.2*/
			/*int x = 2, y = 0;
			try{
				System.out.println(x/y);
			}
			catch (Exception e){
				System.out.println("Une exception a été capturée");
			}
			System.out.println("Fin du programme");*/
			/* On constate que le programme fonctionne et que le system.out.println fonctionne. */
			/*Exercice 1.3*/
			//	int x = 2, y = 0;
			//try{
			//System.out.println("y/x = " + y/x);
			//System.out.println("x/y = " + x/y);
			//System.out.println("Commande de fermeture du programme");
			//}
			//catch (Exception e){
			//System.out.println("Une exception a été capturée");
			/* Ont peut donc remarqué que l'execustion de l'emsemble de ses instructions ne fonctionnent pas.*/
			/*celle qui sont executés se trouvent dans catch{...}*/

			//}
			/*Exercice 1.4*/
			int x = 2, y = 0;
			try{
				System.out.println("y/x = " + y/x);
				System.out.println("x/y = " + x/y);
			}
			catch (ArithmeticException e){
				System.out.println("Une exception a été capturée");
				System.out.println("Exception : " + e.getMessage());/*Exercice 1.5*/
				/*e.getMessage permet d'affcher le message d'erreur dans la console.*/
			}
			finally {
				System.out.println("Commande de fermeture du programme");
			}
			System.out.println("Fin du programme");
		}
	}
