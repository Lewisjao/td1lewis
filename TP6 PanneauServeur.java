package tp6;

	import java.awt.BorderLayout;
	import java.awt.Container;
	import java.awt.event.*;
	import java.net.ServerSocket;
	import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JScrollPane;
	import javax.swing.JTextArea;
	
	public class PanneauServer extends JFrame implements ActionListener{
		//Attributs
		JButton bouton = new JButton("Exit"); 
		JTextArea text= new JTextArea("Le panneau est actif");
		JScrollPane s = new JScrollPane(text);
		//Constructeurs

		public PanneauServer () { 
			super();
			this.setTitle("Serveur - Panneau d'affichage"); 
			this.setSize(400,300); 
			this.setLocation(20,20); 
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 
			Container panneau = getContentPane(); 
			panneau.add(bouton, BorderLayout.SOUTH);
			panneau.add(s);
			bouton.addActionListener(this);

			System.out.println("Le panneau est actif !"); 		
			this.setVisible(true);

			ServerSocket monServerSocket;
			text.append("\n Serveur démarré");
			try {
				monServerSocket = new ServerSocket(8888);
				System.out.println("ServerSocket: " + monServerSocket);
				monServerSocket.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		//Méthodes
		public void actionPerformed (ActionEvent e){ 
			System.out.println("Une action a été détectée");
			System.exit(-1);
		}
		public void ecouter() {
			
		}
		
		//Méthodes main
		public static void main(String[] args){
			PanneauServer p = new PanneauServer ();

		}
	}

