package tp1;

public class Point {

	public int x, y;
	
	public Point(int x1,int y1) {
		
		x=x1;
		y=y1;
		
	}
	
	public int getX() {
		return x;
		
	}
	
	public void setX(int x) {
		this.x=x;
		
	}
	
	public int getY() {
		return y;
	}
}
